import {createElement, addClass, removeClass} from "./helper.mjs";

const USER_KEY = 'BSA_keyboardRunner';
const roomsWrp = document.getElementById("room-list-wrp");
const singleRoomWrp = document.getElementById("single-room-wrp");
const roomsContainer = document.getElementById("rooms-container");
const createRoomBtn = document.getElementById("create-room");
const usersContainer = document.getElementById("users-container");
const backToRoomsBtn = document.getElementById("back-btn");

if (!isUserExist()) {
  navigateTo();
}

const username = sessionStorage.getItem(USER_KEY);
const socket = io(`http://localhost:3002/game?username=${username}`);
let roomsNameExceptions = [];

createRoomBtn.addEventListener('click', (event) => {
  const roomName = prompt('Введите имя комнаты');
  if (!roomName) {
    return;
  }
  if (!validateRoomName(roomName).isValid) {
    alert(validateRoomName(roomName).error);
    return;
  }

  socket.emit("CREATE_ROOM", roomName);
  socket.emit("JOIN_ROOM", {roomName, username});
})
backToRoomsBtn.addEventListener('click', (event) => {
  addClass(singleRoomWrp, "disabled");

  const roomName = getRoomName();
  socket.emit("LEAVE_ROOM", {roomName, username});

  removeClass(roomsWrp, "disabled");
})

function validateRoomName(roomName = '', exceptions = roomsNameExceptions) {
  if (exceptions.includes(roomName)) {
    return {isValid: false, error: 'Такое имя уже занято'};
  }
  if (roomName.trim().length > 2 && roomName.trim().length < 12) {
    return {isValid: true};
  }
  return {isValid: false, error: 'Мин/макс количество символов: 3/12'};
}
function isUserExist() {
  return sessionStorage.getItem(USER_KEY);
}
function userAlreadyActive(data) {
  alert(`В системе уже есть активный пользователь с таким именем: ${data.username}`);
  clearStorage();
  navigateTo();
}
function clearStorage() {
  sessionStorage.removeItem(USER_KEY);
}
function navigateTo(pageName = 'login') {
  window.location = `/${pageName}`;
}

const render = (container, template, place = `beforeend`) => {
  container.insertAdjacentHTML(place, template);
};
const onJoinRoom = evt => {
  const roomName = evt.target.dataset.id;
  console.log('roomId: ', roomName);
  socket.emit("JOIN_ROOM", {roomName, username});
};
const logMessage = data => {
  console.log('Log: ', data);
};
const generateRoom = (roomName, roomUsers = []) => {
  return (
    `<li class="room-item hand-drawn">
      <div class="room flex-column">
        <div class="room__active-users">
          <span>${roomUsers.length} / 4</span> user(s) connected
        </div>
        <div class="user__number flex-centered">${roomName}</div>
        <button class="main-btn no-select hand-drawn" data-id="${roomName}">Join</button>
      </div>
    </li>`
  );
};
const generateUser = (userName) => {
  const isCurrentUser = userName === username;
  return (
    `<li class="user">
      <div class="user__header">
        <div class="user__status"></div>
        <div class="user__name">${userName}${isCurrentUser ? ' (you)' : ''}</div>
      </div>
      <div class="user__progress hand-drawn"></div>
    </li>`
  );
};
const updateRooms = (rooms = []) => {
  roomsNameExceptions = rooms.map(room => room.name);
  roomsContainer.innerHTML = '';
  for (const room of rooms) {
    render(roomsContainer, generateRoom(room.name, room.users));
    const roomButton = roomsContainer.querySelector(`[data-id='${room.name}']`);
    roomButton.addEventListener('click', onJoinRoom)
  }
};
const updateRoomName = roomName => {
  const singleRoomName = document.getElementById("singleRoomName");
  singleRoomName.dataset.name = roomName;
  singleRoomName.innerText = roomName;
};
const getRoomName = () => {
  const singleRoomName = document.getElementById("singleRoomName");
  return singleRoomName.dataset.name;
}
const updateRoomUsersList = users => {
  for (const user of users) {
    render(usersContainer, generateUser(user));
  }
};
const joinRoomDone = ({ roomName, users }) => {
  addClass(roomsWrp, "disabled");

  updateRoomName(roomName);
  updateRoomUsersList(users);

  removeClass(singleRoomWrp, "disabled");
};


socket.on("CONNECT_USER", logMessage);
socket.on("ALREADY_ACTIVE_USER", userAlreadyActive);
socket.on("UPDATE_ROOMS", updateRooms);
socket.on("JOIN_ROOM_DONE", joinRoomDone);
