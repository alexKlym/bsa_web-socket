import roomsList from "./roomsList";

export default io => {
  roomsList(io.of("/game"));
};
