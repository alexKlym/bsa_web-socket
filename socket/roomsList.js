const users = ['default_user'];
const rooms = [{name: 'Default_room', users: []}, {name: 'Default_room2', users: ['default_user']}];
const usersMap = new Map(users.map(username => [username, username]));
const roomsMap = new Map(rooms.map(room => [room.name, room.users]));

const getCurrentRoomName = socket => Object.keys(socket.rooms).find(roomName => roomsMap.has(roomName));
const isUserAlreadyActive = username => {
  return usersMap.has(username)
};
const removeUser = (username) => {
  usersMap.delete(username);
};
const isRoomNameExist = (roomName) => {
  return roomsMap.has(roomName);
};
const updateRooms = (roomName, roomUsers = []) => {
  rooms.push({name: roomName, users: roomUsers});
  roomsMap.set(roomName, roomUsers);
};
const addUserToRoom = (roomName, username) => {
  const userRoom = rooms.find(room => room.name === roomName);
  userRoom.users.push(username);
};
const removeUserFromRoom = (roomName, username) => {
  const userRoom = rooms.find(room => room.name === roomName);
  const userIdx = userRoom.users.findIndex(username);
  userRoom.users.splice(userIdx, 1);
};
const usersInRoom = roomName => {
  return rooms.find(room => room.name === roomName).users;
};

export default io => {
  io.on("connection", socket => {
    const username = socket.handshake.query.username;
    console.log('Server | try to connect user: ', username);
    if (isUserAlreadyActive(username)) {
      socket.emit("ALREADY_ACTIVE_USER", {
        username
      });
      return;
    }
    usersMap.set(username, username);
    socket.emit("CONNECT_USER", {
      username
    });
    socket.emit("UPDATE_ROOMS", rooms);

    socket.on('disconnect', () => {
      removeUser(username);
    });
    socket.on("CREATE_ROOM", roomName => {
      // if (isRoomNameExist(roomName)) {
      //   console.log('isRoomNameExist');
      // }
      updateRooms(roomName);
      socket.emit("UPDATE_ROOMS", rooms);
    });
    socket.on("JOIN_ROOM", ({roomName, username}) => {
      const prevRoomName = getCurrentRoomName(socket);
      if (roomName === prevRoomName) {
        return;
      }
      if (prevRoomName) {
        socket.leave(prevRoomName);
      }
      addUserToRoom(roomName, username);
      socket.join(roomName, () => {
        const users = usersInRoom(roomName);
        io.to(socket.id).emit("JOIN_ROOM_DONE", { roomName, users });
      });
      socket.emit("UPDATE_ROOMS", rooms);
    });
    socket.on('LEAVE_ROOM', ({roomName, username}) => {
      const rName = getCurrentRoomName(socket);
      removeUserFromRoom(roomName, username);
      socket.leave(rName);
      socket.emit("UPDATE_ROOMS", rooms);
    })
  });
};
